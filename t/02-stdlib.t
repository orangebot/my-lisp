#!./repl

; Load the TAP functions
(load "t/tap.lisp")

(diag "# Check if the standard library is functioning correctly")

; Load in the Standard Library
(load "stdlib.lisp")


;  Make sure the basic Standard Library atoms loaded properly.
(ok true "Atom 'true'")
(nok false "Atom 'false'")
(is nil {} "Atom 'nil'")


; Check "func"
(func {check-func msg}
	{pass msg})
(check-func "Function 'func'")


; Check "fst"
(is (fst {1 2 3 4}) 1 "'fst' got the first element")
(is (fst nil) nil "'fst' returns nil on an empty list")


; Check list functions
(is
	(map (fun {x} {* 2 x}) {1 2 3 4})
	{2 4 6 8}
	"map can modify a list")

(is
	(grep (fun {x} {< x 2}) {0 1 2 3})
	{0 1}
	"grep can search through a list")

(is
	(dup 1 4)
	{1 1 1 1}
	"dup can make a list")


; Try to get the nth element in a list
(func {check-nth x}
	{is (nth x {0 1 2 3 4 5}) x (sjoin "'nth' can find element " (num2str x))})
(map check-nth {0 1 2 3 4 5})


; Check if 'len' gives the correct length
(is (len {1 2 3 4 5}) 5 "Length of a list via 'len'")
(is (len nil) 0 "'len' works on nil")


; Make sure 'last' gives the actual last item
(is (last 1 2 3 4 5 6) 6 "'last' gives the last element")


; Check if 'do' can do multiple things.
(do (pass "Do can do one thing"))
(do
	(def {i} 1)
	(def {i} (+ i 1))
	(def {i} (+ i 1))
	(is i 3 "Do can do multiple things"))


; Make sure assigning local variables in 'let' works.
(let {do
	(= {var} 123)
	(is var 123 "local vars works in 'let'")})


; Check that unpack works
(is (unpack + {2 2 2 2 2}) 10 "Unpack works")


; Check that pack works
(is (pack len 2 2 2 2 2) 5 "Pack works")


; Check the logical operators
(nok (not true) "Not true")
(ok (not false) "Not false")

(ok (or true true) "todo - 'or true true' is not the same as just 'true'")
(ok (or true false) "or true false")
(ok (or false true) "or false true")
(nok (or false false) "or false false")

(ok (and true true) "and true true")
(nok (and true false) "and true false")
(nok (and false true) "and false true")
(nok (and false false) "and false false")


; 'foldl' should join each item from right to left.
(is 
	(foldl sjoin "." (split "correct order"))
	"correct order."
	"Function 'foldl' reaches each element")


; Check math functions
(is (sum {2 2 2 2 2}) 10 "sum works on a list")
(is (product {2 2 2}) 8 "product works on a list")

(is (pow 2 2) 4 "2^2 was correct")
(is (pow 5 5) 3125 "5^5 was correct")
(is (pow 4 -2) 0.0625 "4^(-2) was correct")
(is (pow 2 -3) 0.125 "2^(-3) was correct")


; Check the logic of 'select'.
(select
	{(== 3 3) (pass "Got the first item in a select")}
	{(== 3 4) (fail "Got the second item in a select")})

(select
	{(== 3 4) (fail "Got the first item in a select")}
	{(== 3 3) (pass "Got the second item in a select")})

(select
	{(== 3 3) (pass "Got the first item in a select")}
	{(== 3 3) (fail "Got the second item in a select")})


; Make sure 'case' sees each item.
(func {check-case x}
	{is (case x {1 1} {2 2} {3 3} {4 4}) x (sjoin "case returned element " (num2str x))})
(map check-case {1 2 3 4})


(eval tests-run)
