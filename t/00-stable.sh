#!/bin/bash

#Load in the TAP functions
. t/tap.sh

echo "# Make sure builtin functions work properly"

# Make sure that printing works the way we expect.
check <<END
(say "ok # Check say builtin")
END


# Make sure that eval works.
check <<END
(eval {say "ok # Eval works"})
END


# Make sure `if` works right.
check <<END
(if 1
	{say "ok # Check if function"}
	{say "not ok # Check if function"})
(if 0
	{say "not ok # Check if-else function"}
	{say "ok # Check if-else function"})
END


# Check variables
check <<END
(def {ok} "ok # Variables")
(say ok)
END


# Check functions
check <<END
((fun {x} {say x}) "ok # Functions")
END


#Check the comparison operators
check <<END
(if (== 1 1)
	{say "ok # == can compare the same number to true"}
	{say "not ok # == can compare the same number to true"})
(if (== 1 0)
	{say "not ok # == can compare different number to false"}
	{say "ok # == can compare different number to false"})


(def {ok}
	(fun {x y msg}
		{if (== x y)
			{say (sjoin "ok # " msg)}
			{say (sjoin "not ok # " msg)}}))

(ok (! 0) 1 "! can invert true")
(ok (! 1) 0 "! can invert false")
(ok (> 2 1) 1 "large > small")
(ok (< 2 1) 0 "large < small")
(ok (> 1 2) 0 "small > large")
(ok (< 1 2) 1 "small < large")
(ok (>= 5 5) 1 "same >= same")
(ok (<= 5 5) 1 "same <= same")

(ok (== {} {}) 1 "Compare empty Q-Exprs")
(ok (== {1} {}) 0 "Compare an empty Q-Expr to a non-empty")
(ok (== {} {1}) 0 "Compare an empty Q-Expr to a non-empty")
(ok (== {1} {1}) 1 "Compare the same non-empty Q-Exprs")
(ok (== {1 2} {1 2 3}) 0 "Compare Q-Exprs with different elements")

(ok (== "a" "a") 1 "Compare the same string")
(ok (== "a" "b") 0 "Compare diferent strings")
(ok (== "b" "a") 0 "Compare diferent strings")
(ok (== "abc" "ab") 0 "Compare different sized strings")
(ok (== "ab" "abc") 0 "Compare different sized strings")
END


check <<END
(if (== {1 2} (join {1} {2}))
	{say "ok # Check join function"}
	{say "not ok # Check join function"})
END


check <<END
(if (== {1} (head {1 2}))
	{say "ok # Check head function"}
	{say "not ok # Check head function"})
END


check <<END
(if (== {2} (tail {1 2}))
	{say "ok # Check tail function"}
	{say "not ok # Check tail function"})
END


# Check joining strings
check <<END
(say (sjoin "o" "k" " # Joining strings"))
END


check <<END
(if (== {"a" "b" "c"} (ssplit "abc"))
	{say "ok # Check ssplit function"}
	{say "not ok # Check ssplit function"})
END


#Check the math functions work corectly.
check <<END
(def {ok}
	(fun {x y msg}
		{if (== x y)
			{say (sjoin "ok # " msg)}
			{say (sjoin "not ok # " msg)}}))
(ok (+ 1 1) 2 "Add numbers")
(ok (+ 0.1 0.2) 0.3 "todo - Problem with floating point numbers")
(ok (- 7 2) 5 "Subtract numbers")
(ok (- 2 7) -5 "Subtract to negative")
(ok (- 0.3 0.2) 0.1 "todo - Problem with floating point numbers")
(ok (* 2 5) 10 "Multiply numbers")
(ok (* 1.5 10) 15 "Multiply decimals")
(ok (/ 4 4) 1 "divide numbers")
(ok (/ 1 2) 0.5 "divide to decimal")
END


check <<END
(def {ok}
	(fun {x y}
		{if (== x y)
			{say (sjoin "ok # Can convert " y " to a string")}
			{say (sjoin "not ok # Expected " y ", but got " x)}}))
(ok (num2str 1) "1")
(ok (num2str 100) "100")
(ok (num2str 100000000) "100000000")
(ok (num2str 5.125) "5.125")
(ok (num2str 0.85) "0.85")
(ok (num2str 0.1000000) "0.1")
(ok (num2str 0.00000001) "1e-08")
END


check <<END
(def {ok}
	(fun {x y}
		{if (== x y)
			{say "ok # Got the correct environment"}
			{say "not ok # 'env' returned: " x}}))
;Root environment
(ok ((fun {x} {env x}) 1) {list head tail eval join err print say ssplit sjoin num2str + - * / == != ! < > <= >= if def = env fun load ok})
;Current environment
(ok ((fun {x} {env x}) 0) {x list head tail eval join err print say ssplit sjoin num2str + - * / == != ! < > <= >= if def = env fun load ok})
END
