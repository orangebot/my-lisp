#!./repl

; Make sure that the TAP framework works properly.

; Load in the TAP framework
(load "t/tap.lisp")

(pass "'pass' returns ok")

(ok 1 "'ok' returns ok on true")

(nok 0 "'nok' returns ok on false")

(is 5 5 "'is' returns ok when values are the same")

(isnt 3 4 "'isnt' returns ok when values are different")

(say "1.." *tests*)
