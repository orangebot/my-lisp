#!/bin/bash

# Make a var to keep track of the number of run tests
tests=0

echo "# Running basic stability checks"

#Temp files
tmpdir=$TESTTMP
in=$tmpdir/lisp.in
out=$tmpdir/lisp.out

if test ! -n "$tmpdir"; then
	tmpdir=/tmp/`whoami`
fi
mkdir -p $tmpdir
touch $in
touch $out


clean () {
	echo "1..$tests"
	rm $in $out
}

trap clean 0 1 2 3 6


#Function to make throwing 'not ok' errors easier.
test-error () {
	echo "not ok #" $1
	echo "# Given:"
	sed 's/^/# /g' < $in
	echo "# Returned:"
	sed 's/^/# /g' < $out
	echo "#"
}


check () {
	#Take in the lisp code and store it in a tmp file
	cat > $in
	#Feed the code into the program and grab the output
	./repl $in > $out
	
	#Count the number of tests run
	run=`grep -e '^ok ' -e '^not ok ' $out | wc -l`
	
	#Check if the process died
	if test $? -ne 0; then
		test-error "Died with non-zero exit code"
	
	#Check if the process threw an error
	elif grep -q '^Error:' $out; then
		test-error "Process returned an error"
	
	#If no tests were run, report it
	elif test "$run" -eq 0; then
		test-error "A check was run, but no result was found"
	
	else
		cat $out
	fi
	
	#If no tests were run, add one
	if test "$run" -eq 0; then run=1; fi
	
	#Add the tests to to the running total
	tests=`echo $tests + $run | bc`
}
