; Build up functions to make testing much easier. Test output should
; match TAP standards: http://testanything.org/tap-specification.html

; Make a global to keep track of the number of tests run.
(def {*tests*} 0)

; Return the number of tests run
(def {tests-run}
	{say "1.." *tests*})

; Make functions easier
(def {func}
	(fun {name body}
		{def (head name) (fun (tail name) body)}))

; Do multiple things
(func {do & tasks} {tasks})

; Return a diagnostic message
(func {diag msg}
	{say (sjoin "# " msg)})

; Pass a test
(func {pass msg}
	{do
		(def {*tests*} (+ 1 *tests*))
		(say (sjoin "ok # " msg))})

; Fail a test
(func {fail msg}
	{do
		(def {*tests*} (+ 1 *tests*))
		(say (sjoin "not ok # " msg))})

; Return ok if true
(func {ok x msg}
	{if x {pass msg} {fail msg}})

; Return ok if false
(func {nok x msg}
	{ok (! x) msg})

; Return ok if two values are the same
(func {is x y msg}
	{ok (== x y) msg})

; Return ok if two values are not the same
(func {isnt x y msg}
	{ok (!= x y) msg})

