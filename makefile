include config.mk

all: options repl

clean:
	rm -f repl linenoise.* mpc.* *.o

options:
	@echo CC = $(CC)
	@echo CFLAGS = $(CCFLAGS)
	@echo LDFLAGS = $(CLDFLAGS)

repl.o: repl.c linenoise.h mpc.h
	$(CC) $(CCFLAGS) -c repl.c

repl: repl.o linenoise.o mpc.o
	$(CC) $(CLDFLAGS) -o repl repl.o linenoise.o mpc.o

test: repl
	prove -f -j1 t/00-stable.sh
	prove -W -f t/

install: test
	mkdir -p $(PREFIX)/bin
	cp -f repl $(PREFIX)/bin
	chmod 755 $(PREFIX)/bin/repl

commit: .commit
	@grep '^VERSION' config.mk
	test ! $$(git tag -l $(VERSION))
	git commit -F .commit
	git tag $(VERSION)
	touch .commit



#Compile the Linenoise library and bring it into the local dir.
linenoise.c: linenoise/linenoise.c
	cp linenoise/linenoise.c linenoise.c

linenoise.h: linenoise/linenoise.h
	cp linenoise/linenoise.h linenoise.h

linenoise.o: linenoise.c linenoise.h
	$(CC) -c -o linenoise.o linenoise.c



#Compile the MPC library and bring it into the local dir.
mpc.c: mpc/mpc.c
	cp mpc/mpc.c mpc.c

mpc.h: mpc/mpc.h
	cp mpc/mpc.h mpc.h

mpc.o: mpc.c mpc.h
	$(CC) -c -o mpc.o mpc.c
