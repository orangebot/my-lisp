#include <stdio.h>
#include <stdlib.h>

/* Grab the linenoise library from the included subtree. */
#include "linenoise.h"

/* Grab the mpc library from the included subtree. */
#include "mpc.h"






/* Define some macros. */
/* Macro to make error checking smaller and easier. */
#define LASSERT(args, cond, fmt, ...) \
	if (!(cond)) { \
		lval* err = lval_err(fmt, ##__VA_ARGS__); \
		lval_del(args); \
		return err; \
	}

/* Macro to check for the number of arguments passed. */
#define LASSERT_NUM(func_name, lval_arg, num_args) \
	LASSERT(lval_arg, lval_arg->count == num_args, \
		"Function '%s' got %i args, but expected %i.", \
		func_name, lval_arg->count, num_args)

/* Macro to check for the type. */
#define LASSERT_TYPE(func_name, lval_arg, cell_num, lval_type) \
	LASSERT(lval_arg, lval_arg->cell[cell_num]->type == lval_type, \
		"Function '%s' passed a %s, but needs a %s.", \
		func_name, ltype_name(lval_arg->cell[cell_num]->type), ltype_name(lval_type))







typedef struct lval lval;
typedef struct lenv lenv;

/* Make a function pointer type to represent language functions. */
typedef lval*(*lbuiltin)(lenv*, lval*);

/* Make a struct to hold values or errors. */
struct lval {
	int type;
	
	/* Basics */
	double num;
	char* err;
	char* sym;
	char* str;
	
	/* Functions */
	lbuiltin builtin;
	lenv* env;
	lval* formals;
	lval* body;
	
	/* Count of and pointer to a list of 'lval*'. */
	int count;
	lval** cell;
};

/* Make a struct to keep a list of relationships between names and values. */
struct lenv {
	lenv* par;
	int count;
	char** syms;
	lval** vals;
};

/* Possible lval types. */
enum {
	LVAL_ERR,
	LVAL_FUN,
	LVAL_NUM,
	LVAL_QEXPR,
	LVAL_SEXPR,
	LVAL_STR,
	LVAL_SYM,
};

/* Possible error types. */
enum { LERR_DIV_ZERO, LERR_BAD_OP, LERR_BAD_NUM };





/* Define the parser pointers. */
mpc_parser_t* Comment;
mpc_parser_t* Number;
mpc_parser_t* Symbol;
mpc_parser_t* String;
mpc_parser_t* Sexpr;
mpc_parser_t* Qexpr;
mpc_parser_t* Expr;
mpc_parser_t* Lispy;





/* Make some prototypes. */
lval* lval_num(double);
lval* lval_err(char*, ...);
lval* lval_sym(char*);
lval* lval_sexpr(void);
lval* lval_qexpr(void);
lval* lval_fun(char*, lbuiltin);
lval* lval_lambda(lval*, lval*);
lval* lval_str(char*);

char* ltype_name(int);
void lval_del(lval*);
lval* lval_add(lval*, lval*);
lval* lval_read(mpc_ast_t*);
lval* lval_read_num(mpc_ast_t*);
lval* lval_read_str(mpc_ast_t*);
void lval_say_str(lval*);
void lval_print_str(lval*);
void lval_print(lval*, int);
void lval_println(lval*, int);
void lval_print_expr(lval*, int, char, char);
lval* lval_call(lenv*, lval*, lval*);
int lval_eq(lval*, lval*);

lval* lval_eval_sexpr(lenv*, lval*);
lval* lval_eval(lenv*, lval*);
lval* lval_pop(lval*, int);
lval* lval_take(lval*, int);
lval* lval_join(lval*, lval*);
lval* lval_copy(lval*);

lenv* lenv_new(void);
void lenv_del(lenv*);
lval* lenv_get(lenv*, lval*);
void lenv_put(lenv*, lval*, lval*);
void lenv_def(lenv*, lval*, lval*);
lenv* lenv_copy(lenv*);
void lenv_add_builtin(lenv*, char*, lbuiltin);
void lenv_add_builtins(lenv*);

lval* builtin_head(lenv*, lval*);
lval* builtin_tail(lenv*, lval*);
lval* builtin_list(lenv*, lval*);
lval* builtin_eval(lenv*, lval*);
lval* builtin_join(lenv*, lval*);
lval* builtin_err(lenv*, lval*);

lval* builtin_print(lenv*, lval*);
lval* builtin_say(lenv*, lval*);
lval* builtin_ssplit(lenv*, lval*);
lval* builtin_sjoin(lenv*, lval*);
lval* builtin_num2str(lenv*, lval*);

lval* builtin_op(lenv*, lval*, char*);
lval* builtin_add(lenv*, lval*);
lval* builtin_sub(lenv*, lval*);
lval* builtin_mul(lenv*, lval*);
lval* builtin_div(lenv*, lval*);

lval* builtin_cmp(lenv*, lval*, char*);
lval* builtin_eq(lenv*, lval*);
lval* builtin_ne(lenv*, lval*);
lval* builtin_not(lenv*, lval*);

lval* builtin_ord(lenv*, lval*, char*);
lval* builtin_gt(lenv*, lval*);
lval* builtin_lt(lenv*, lval*);
lval* builtin_ge(lenv*, lval*);
lval* builtin_le(lenv*, lval*);
lval* builtin_if(lenv*, lval*);

lval* builtin_lambda(lenv*, lval*);
lval* builtin_var(lenv*, lval*, char*);
lval* builtin_def(lenv*, lval*);
lval* builtin_put(lenv*, lval*);
lval* builtin_load(lenv*, lval*);
lval* builtin_env(lenv*, lval*);
lval* builtin(lenv*, lval*, char*);






/* Make a new number type lval. */
lval* lval_num(double x) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_NUM;
	v->num = x;
	return v;
}


/* Make a new error type lval. */
lval* lval_err(char* fmt, ...) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_ERR;
	
	/* Make a new va_list and init it. */
	va_list va;
	va_start(va, fmt);
	
	/* Allocate 512 bytes of space. */
	v->err = malloc(512);
	
	/* Print the error string with a max of 511 chars. */
	vsnprintf(v->err, 511, fmt, va);
	
	/* Shrink the size to the actual size of the string. */
	v->err = realloc(v->err, strlen(v->err) + 1);
	
	/* Cleanup the va_list. */
	va_end(va);
	
	return v;
}


/* Make a new symbol type lval. */
lval* lval_sym(char* s) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_SYM;
	v->sym = malloc(strlen(s) + 1);
	strcpy(v->sym, s);
	return v;
}


/* Make a new S-Expression type lval. */
lval* lval_sexpr(void) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_SEXPR;
	v->count = 0;
	v->cell = NULL;
	return v;
}


/* Make a new Q-Expression type lval. */
lval* lval_qexpr(void) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_QEXPR;
	v->count = 0;
	v->cell = NULL;
	return v;
}


/* Make a new function type lval. */
lval* lval_fun(char* name, lbuiltin func) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_FUN;
	v->sym = malloc(strlen(name) + 1);
	strcpy(v->sym, name);
	v->builtin = func;
	return v;
}


/* Constructor for functions. */
lval* lval_lambda(lval* formals, lval* body) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_FUN;
	
	char* name = "lambda";
	v->sym = malloc(strlen(name) + 1);
	strcpy(v->sym, name);
	
	/* Set 'builtin' to NULL. */
	v->builtin = NULL;
	
	/* Built the new env. */
	v->env = lenv_new();
	
	/* Set the formals and body. */
	v->formals = formals;
	v->body = body;
	
	return v;
}


/* Make a constructor for strings. */
lval* lval_str(char* s) {
	lval* v = malloc(sizeof(lval));
	v->type = LVAL_STR;
	v->str = malloc(strlen(s) + 1);
	strcpy(v->str, s);
	return v;
}






/* Convert the enum type to a text string. */
char* ltype_name(int i) {
	switch(i) {
		case LVAL_FUN: return "Function";
		case LVAL_NUM: return "Number";
		case LVAL_ERR: return "Error";
		case LVAL_SYM: return "Symbol";
		case LVAL_STR: return "String";
		case LVAL_SEXPR: return "S-Expression";
		case LVAL_QEXPR: return "Q-Expression";
		default: return "Unknown";
	}
}


/* Takes an intem at index `i`, removes it from the list, and returns it. */
lval* lval_pop(lval* v, int i) {
	/* Find the item. */
	lval* x = v->cell[i];
	
	/* Shift over the memory at `i` to remove it from the set. */
	memmove(&v->cell[i], &v->cell[i+1], sizeof(lval*) * (v->count - i - 1));
	
	/* Decrease the count of items. */
	v->count--;
	
	/* Reallocate the memory. */
	v->cell = realloc(v->cell, sizeof(lval*) * v->count);
	
	return x;
}


/* Like `lval_pop`, but will delete the structure that is left. */
lval* lval_take(lval* v, int i) {
	lval* x = lval_pop(v, i);
	lval_del(v);
	return x;
}


/* Delete an lval. */
void lval_del(lval* v) {
	switch (v->type) {
		case LVAL_NUM:
			break;
			
		case LVAL_ERR:
			free(v->err);
			break;
		
		case LVAL_STR:
			free(v->str);
			break;
			
		case LVAL_SYM:
			free(v->sym);
			break;
			
		case LVAL_SEXPR:
		case LVAL_QEXPR:
			for (int i = 0; i < v->count; ++i) {
				lval_del(v->cell[i]);
			}
			free(v->cell);
			break;
			
		case LVAL_FUN:
			free(v->sym);
			if (!v->builtin) {
				lenv_del(v->env);
				lval_del(v->formals);
				lval_del(v->body);
			}
			break;
	}
	
	/* Free the memory from the struct. */
	free(v);
}


/* Function to take all of the elements from one lval to another. */
lval* lval_join(lval* x, lval* y) {
	/* For each cell in 'y' add it to 'x'. */
	while (y->count) {
		x = lval_add(x, lval_pop(y, 0));
	}
	
	/* Delete the empty 'y' and return 'x'. */
	lval_del(y);
	return x;
}


/* Function to copy an lval. */
lval* lval_copy(lval* v) {
	lval* x = malloc(sizeof(lval));
	
	x->type = v->type;
	
	switch (v->type) {
		/* Copy functions and numbers directly. */
		case LVAL_FUN:
			x->builtin = v->builtin;
			x->sym = malloc(strlen(v->sym) + 1);
			strcpy(x->sym, v->sym);
			if (v->builtin) {
				x->builtin = v->builtin;
			} else {
				x->builtin = NULL;
				x->env = lenv_copy(v->env);
				x->formals = lval_copy(v->formals);
				x->body = lval_copy(v->body);
			}
			break;
		
		case LVAL_NUM:
			x->num = v->num;
			break;
		
		/* Copy strings using malloc and strcpy. */
		case LVAL_ERR:
			x->err = malloc(strlen(v->err) + 1);
			strcpy(x->err, v->err);
			break;
		
		case LVAL_SYM:
			x->sym = malloc(strlen(v->sym) + 1);
			strcpy(x->sym, v->sym);
			break;
		
		/* Copy lists by copying each sub-expression. */
		case LVAL_SEXPR:
		case LVAL_QEXPR:
			x->count = v->count;
			x->cell = malloc(sizeof(lval*) * x->count);
			for (int i = 0; i < x->count; ++i) {
				x->cell[i] = lval_copy(v->cell[i]);
			}
			break;
		
		case LVAL_STR:
			x->str = malloc(strlen(v->str) + 1);
			strcpy(x->str, v->str);
			break;
	}
	
	return x;
}







lval* lval_add(lval* v, lval* x) {
	v->count++;
	v->cell = realloc(v->cell, sizeof(lval*) * v->count);
	v->cell[v->count - 1] = x;
	return v;
}


/* Read in a number from the AST. */
lval* lval_read_num(mpc_ast_t* t) {
	errno = 0;
	double x = strtod(t->contents, NULL);
	return (errno != ERANGE)
		? lval_num(x)
		: lval_err("invalid number");
}


lval* lval_read_str(mpc_ast_t* t) {
	/* Cut off the final quote char. */
	t->contents[ strlen(t->contents) - 1 ] = '\0';
	
	/* Copy the string missing out the first quote char. */
	char* unescaped = malloc( strlen(t->contents + 1) + 1 );
	strcpy(unescaped, t->contents + 1);
	
	/* Pass it through the unescape function. */
	unescaped = mpcf_unescape(unescaped);
	
	/* Construct a new lval using the string. */
	lval* str = lval_str(unescaped);
	
	/* Clean up and return. */
	free(unescaped);
	return str;
}


/* Read in a part of the AST and turn it into an 'lval*'. */
lval* lval_read(mpc_ast_t* t) {
	/* Convert if a number of symbol. */
	if (strstr(t->tag, "number")) { return lval_read_num(t); }
	if (strstr(t->tag, "symbol")) { return lval_sym(t->contents); }
	if (strstr(t->tag, "string")) { return lval_read_str(t); }
	
	/* If it's the root or an sexpr then create an empty list. */
	lval* x = NULL;
	if (strcmp(t->tag, ">") == 0) { x = lval_sexpr(); }
	if (strstr(t->tag, "sexpr")) { x = lval_sexpr(); }
	if (strstr(t->tag, "qexpr")) { x = lval_qexpr(); }
	
	/* Make a list with any valid expression. */
	for (int i = 0; i < t->children_num; ++i) {
		/* Parts of the lang to ignore. */
		if (strcmp(t->children[i]->contents, "(") == 0) { continue; }
		if (strcmp(t->children[i]->contents, "(") == 0) { continue; }
		if (strcmp(t->children[i]->contents, ")") == 0) { continue; }
		if (strcmp(t->children[i]->contents, "{") == 0) { continue; }
		if (strcmp(t->children[i]->contents, "}") == 0) { continue; }
		if (strcmp(t->children[i]->tag, "regex") == 0) { continue; }
		if (strstr(t->children[i]->tag, "comment")) { continue; }
		
		x = lval_add(x, lval_read(t->children[i]));
	}
	
	return x;
}


/* Nicely print out a formatted string. */
void lval_say_str(lval* v) {
	/* Print the string. */
	printf("%s", v->str);
}


void lval_print_str(lval* v) {
	/* Make a copy of the string. */
	char* escaped = malloc(strlen(v->str) + 1);
	strcpy(escaped, v->str);
	
	/* Pass it through the escape function. */
	escaped = mpcf_escape(escaped);
	
	/* Print the string between two `"` chars. */
	printf("\"%s\"", escaped);
	
	/* Clean up. */
	free(escaped);
}


/* Print out the value of an lval. */
void lval_print(lval* v, int raw) {
	switch (v->type) {
		case LVAL_NUM:
			(raw)
				? printf("%f", v->num)
				: printf("%.10g", v->num);
			break;
		
		case LVAL_ERR:
			printf("Error: %s", v->err);
			break;
		
		case LVAL_SYM:
			printf("%s", v->sym);
			break;
		
		case LVAL_STR:
			(raw)
				? lval_print_str(v)
				: lval_say_str(v);
			break;
			
		case LVAL_SEXPR:
			lval_print_expr(v, raw, '(', ')');
			break;
		
		case LVAL_QEXPR:
			lval_print_expr(v, raw, '{', '}');
			break;
		
		case LVAL_FUN:
			if (v->builtin) {
				printf("%s", v->sym);
			} else {
				printf("(fun ");
				lval_print(v->formals, raw);
				putchar(' ');
				lval_print(v->body, raw);
				putchar(')');
			}
			break;
	}
}


/* Print out the value of an lval, but witha new line at the end. */
void lval_println(lval* v, int raw) {
	lval_print(v, raw);
	putchar('\n');
}


/* Print out S-Expressions to make sure it was read in properly. */
void lval_print_expr(lval* v, int raw, char open, char close) {
	putchar(open);
	for (int i = 0; i < v->count; ++i) {
		/* Print the value of the cell. */
		lval_print(v->cell[i], raw);
		
		/* Don't place a trailing space. */
		if (i != (v->count - 1)) {
			putchar(' ');
		}
	}
	putchar(close);
}

/* Function to build a function in the environment. */
lval* lval_call(lenv* e, lval* f, lval* a) {
	/* If it's a builtin, just return it. */
	if (f->builtin) { return f->builtin(e, a); }
	
	/* Record argument counts */
	int given = a->count;
	int total = f->formals->count;
	
	/* While arguments still remain. */
	while (a->count) {
		/* If we run out of formal arguments, throw an error. */
		if (f->formals->count == 0) {
			lval_del(a);
			return lval_err(
				"Function got %i args, but expected %i.",
				given, total);
		}
		
		/* Pop the first symbol from the formal list. */
		lval* sym = lval_pop(f->formals, 0);
		
		/* Check if the special case '&' is found. If so, make the next
		symbol act as a catch all case. */
		if (strcmp(sym->sym, "&") == 0) {
			/* Make sure there is another symbol after this one. */
			if (f->formals->count != 1) {
				lval_del(a);
				return lval_err(
					"Invalid format. Symbol '&' must be followed by another symbol.");
			}
			
			/* Next formal should be bound to all remaining args. */
			lval* nsym = lval_pop(f->formals, 0);
			lenv_put(f->env, nsym, builtin_list(e, a));
			lval_del(sym);
			lval_del(nsym);
			break;
		}
		
		/* Pop the next arg from the list. */
		lval* val = lval_pop(a, 0);
		
		/* Bind a copy into the env. */
		lenv_put(f->env, sym, val);
		
		/* Delete the symbol and value. */
		lval_del(sym);
		lval_del(val);
	}
	
	/* Arg list is now bound, so clean up. */
	lval_del(a);
	
	/* Bind '&' to an empty list if it still remains. */
	if (f->formals->count > 0 &&
		strcmp(f->formals->cell[0]->sym, "&") == 0) {
		
		if (f->formals->count != 2) {
			lval_del(a);
			return lval_err(
				"Invalid format. Symbol '&' must be followed by another symbol.");
		}
		
		/* Pop and delete the '&' symbol. */
		lval_del(lval_pop(f->formals, 0));
		
		/* Get the next symbol and set it to an empty list. */
		lval* sym = lval_pop(f->formals, 0);
		lval* val = lval_qexpr();
		
		/* Bind to the env and delete. */
		lenv_put(f->env, sym, val);
		lval_del(sym);
		lval_del(val);
	}
	
	/* Eval if all formal args are bound. */
	if (f->formals->count == 0) {
		/* Set the env parent to the current env. */
		f->env->par = e;
		
		/* Eval and return. */
		return builtin_eval(
			f->env,
			lval_add(
				lval_sexpr(),
				lval_copy(f->body)));
	} else {
		/* Return the partially evaluated function. */
		return lval_copy(f);
	}
}


/* Function to init the environment. */
lenv* lenv_new(void) {
	lenv* e = malloc(sizeof(lenv));
	e->par = NULL;
	e->count = 0;
	e->syms = NULL;
	e->vals = NULL;
	return e;
}


/* Function to free memory and clean up the environment. */
void lenv_del(lenv* e) {
	for (int i = 0; i < e->count; ++i) {
		free(e->syms[i]);
		lval_del(e->vals[i]);
	}
	free(e->syms);
	free(e->vals);
	free(e);
}


/* Function to get a variable from the list. */
lval* lenv_get(lenv* e, lval* k) {
	/* Iterate over the items in the list. */
	for (int i = 0; i < e->count; ++i) {
		/* Check if the stored string matches. If so, return the 
		matching value. */
		if (strcmp(e->syms[i], k->sym) == 0) {
			return lval_copy(e->vals[i]);
		}
	}
	
	/* Nothing matched, check the parent. Otherwise return an error. */
	if (e->par) {
		return lenv_get(e->par, k);
	} else {
		return lval_err("Unrecognized symbol '%s'", k->sym);
	}
}


/* Function to put a value in the local environment. */
void lenv_put(lenv* e, lval* k, lval* v) {
	/* Iterate over the items in the list. */
	for (int i = 0; i < e->count; ++i) {
		/* If the variable exists already, replace it with the one given. */
		if (strcmp(e->syms[i], k->sym) == 0) {
			lval_del(e->vals[i]);
			e->vals[i] = lval_copy(v);
			return;
		}
	}
	
	/* The variable does not exist already, so make space and add it. */
	e->count++;
	e->vals = realloc(e->vals, sizeof(lval*) * e->count);
	e->syms = realloc(e->syms, sizeof(char*) * e->count);
	
	/* Copy the values into the new location. */
	e->vals[e->count - 1] = lval_copy(v);
	e->syms[e->count - 1] = malloc( strlen(k->sym) + 1 );
	strcpy(e->syms[e->count - 1], k->sym);
}


/* Function to put a var in the root environment, effectively making a
global. */
void lenv_def(lenv* e, lval* k, lval* v) {
	/* Go to the root env. */
	while (e->par) { e = e->par; }
	/* Put the value in the root. */
	lenv_put(e, k, v);
}


lenv* lenv_copy(lenv* e) {
	lenv* n = malloc(sizeof(lenv));
	
	n->par = e->par;
	n->count = e->count;
	n->syms = malloc( sizeof(char*) * n->count);
	n->vals = malloc( sizeof(lval*) * n->count);
	
	for (int i = 0; i < e->count; ++i) {
		n->syms[i] = malloc( strlen(e->syms[i]) + 1 );
		strcpy(n->syms[i], e->syms[i]);
		n->vals[i] = lval_copy(e->vals[i]);
	}
	
	return n;
}


/* Function to add a function to the environment. */
void lenv_add_builtin(lenv* e, char* name, lbuiltin func) {
	lval* k = lval_sym(name);
	lval* v = lval_fun(name, func);
	lenv_put(e, k, v);
	lval_del(k);
	lval_del(v);
}


/* Function to add the builtin functions to the environment. */
void lenv_add_builtins(lenv* e) {
	/* Add list functions. */
	lenv_add_builtin(e, "list", builtin_list);
	lenv_add_builtin(e, "head", builtin_head);
	lenv_add_builtin(e, "tail", builtin_tail);
	lenv_add_builtin(e, "eval", builtin_eval);
	lenv_add_builtin(e, "join", builtin_join);
	lenv_add_builtin(e, "err", builtin_err);
	
	/* String based stuff. */
	lenv_add_builtin(e, "print", builtin_print);
	lenv_add_builtin(e, "say", builtin_say);
	lenv_add_builtin(e, "ssplit", builtin_ssplit);
	lenv_add_builtin(e, "sjoin", builtin_sjoin);
	lenv_add_builtin(e, "num2str", builtin_num2str);
	
	/* Add math functions. */
	lenv_add_builtin(e, "+", builtin_add);
	lenv_add_builtin(e, "-", builtin_sub);
	lenv_add_builtin(e, "*", builtin_mul);
	lenv_add_builtin(e, "/", builtin_div);
	
	/* Add comparison functions. */
	lenv_add_builtin(e, "==", builtin_eq);
	lenv_add_builtin(e, "!=", builtin_ne);
	lenv_add_builtin(e, "!", builtin_not);
	lenv_add_builtin(e, "<", builtin_lt);
	lenv_add_builtin(e, ">", builtin_gt);
	lenv_add_builtin(e, "<=", builtin_le);
	lenv_add_builtin(e, ">=", builtin_ge);
	lenv_add_builtin(e, "if", builtin_if);
	
	/* Add variable functions. */
	lenv_add_builtin(e, "def", builtin_def);
	lenv_add_builtin(e, "=", builtin_put);
	lenv_add_builtin(e, "env", builtin_env);
	
	/* Add lambda function. */
	lenv_add_builtin(e, "fun", builtin_lambda);
	
	/* Add file functions. */
	lenv_add_builtin(e, "load", builtin_load);
}










/* Check the arguments are good, and evaluate the operators. */
lval* builtin_op(lenv* e, lval* a, char* op) {
	/* Make sure the arguments are numbers. */
	for (int i = 0; i < a->count; ++i) {
		if (a->cell[i]->type != LVAL_NUM) {
			lval_del(a);
			return lval_err("'%s' can not operate on non-number.", op);
		}
	}
	
	/* Pop the first element. */
	lval* x = lval_pop(a, 0);
	
	/* If '-' and no arguments, the negate the value. */
	if ((strcmp(op, "-") == 0) && a->count == 0) {
		x->num = -x->num;
	}
	
	/* While there are still elements remaining perform the correct 
	operation. */
	while (a->count > 0) {
		/* Pop the next element. */
		lval* y = lval_pop(a, 0);
		
		if (strcmp(op, "+") == 0) { x->num += y->num; }
		if (strcmp(op, "-") == 0) { x->num -= y->num; }
		if (strcmp(op, "*") == 0) { x->num *= y->num; }
		if (strcmp(op, "/") == 0) {
			/* Make sure there is no division by zero. */
			if (y->num == 0) {
				lval_del(x);
				lval_del(y);
				x = lval_err("Divison by zero.");
				break;
			}
			x->num /= y->num;
		}
		
		lval_del(y);
	}
	
	lval_del(a);
	return x;
}


/* Check if the arguments are equivalent. True = 1, False = 0 */
int lval_eq(lval* x, lval* y) {
	/* Different types will always return false. */
	if (x->type != y->type) { return 0; }
	
	/* Compare the types based on what type it is. */
	switch (x->type) {
		case LVAL_NUM:
			return (x->num == y->num);
		
		case LVAL_ERR:
			return (strcmp(x->err, y->err) == 0);
		
		case LVAL_SYM:
			return (strcmp(x->sym, y->sym) == 0);
		
		case LVAL_STR:
			return (strcmp(x->str, y->str) == 0);
		
		case LVAL_FUN:
			if (x->builtin || y->builtin) {
				return (x->builtin == y->builtin);
			} else {
				return lval_eq(x->formals, y->formals)
					&& lval_eq(x->body, y->body);
			}
		
		case LVAL_QEXPR:
		case LVAL_SEXPR:
			if (x->count != y->count) { return 0; }
			/* Make sure each element is the same as well. */
			for (int i = 0; i < x->count; ++i) {
				if (!lval_eq(x->cell[i], y->cell[i])) { return 0; }
			}
			return 1;
			break;
	}
	
	/* Catch all false. */
	return 0;
}








/* Function to give the first element in a Q-Expression. */
lval* builtin_head(lenv* e, lval* a) {
	LASSERT_NUM("head", a, 1);
	LASSERT_TYPE("head", a, 0, LVAL_QEXPR);
	LASSERT(a, a->cell[0]->count != 0,
		"Function 'head' passed an empty Q-Expression.");
	
	lval* v = lval_take(a, 0);
	while (v->count > 1) { lval_del( lval_pop(v, 1) ); }
	return v;
}


/* Function to give all but the first element in a Q-Expression. */
lval* builtin_tail(lenv* e, lval* a) {
	LASSERT_NUM("tail", a, 1);
	LASSERT_TYPE("tail", a, 0, LVAL_QEXPR);
	LASSERT(a, a->cell[0]->count != 0,
		"Function 'tail' passed empty Q-Expression.");
	
	lval* v = lval_take(a, 0);
	lval_del( lval_pop(v, 0) );
	return v;
}


/* Function to convert from an S-Expression to a Q-Expression. */
lval* builtin_list(lenv* e, lval* a) {
	a->type = LVAL_QEXPR;
	return a;
}


/* Function to convert from a Q-Expression to an S-Expression, then evaluates
it. */
lval* builtin_eval(lenv* e, lval* a) {
	LASSERT_NUM("eval", a, 1);
	LASSERT_TYPE("eval", a, 0, LVAL_QEXPR);
	
	lval* x = lval_take(a, 0);
	x->type = LVAL_SEXPR;
	return lval_eval(e, x);
}


/* Function to join two Q-Expressions. */
lval* builtin_join(lenv* e, lval* a) {
	for (int i = 0; i < a->count; ++i) {
		LASSERT_TYPE("join", a, i, LVAL_QEXPR);
	}
	
	lval* x = lval_pop(a, 0);
	
	while (a->count) {
		x = lval_join(x, lval_pop(a, 0));
	}
	
	lval_del(a);
	return x;
}


/* Make a function to load in a file, parse, and evaluate it. */
lval* builtin_load(lenv* e, lval* a) {
	LASSERT_NUM("load", a, 1);
	LASSERT_TYPE("load", a, 0, LVAL_STR);
	
	/* Parse the file given. */
	mpc_result_t r;
	if (mpc_parse_contents(a->cell[0]->str, Lispy, &r)) {
		/* Read the file. */
		lval* expr = lval_read(r.output);
		mpc_ast_delete(r.output);
		
		/* Eval each item. */
		while (expr->count) {
			lval * x = lval_eval(e, lval_pop(expr, 0));
			
			/* If there is an error, print it. */
			if (x->type == LVAL_ERR) { lval_println(x, 1); }
			lval_del(x);
		}
		
		/* Clean up the expressions and args. */
		lval_del(expr);
		lval_del(a);
		
		/* Return an empty list. */
		return lval_sexpr();
	} else {
		/* Grab the parsing error as a string. */
		char* err_msg = mpc_err_string(r.error);
		mpc_err_delete(r.error);
		
		/* Build an error message using it. */
		lval* err = lval_err("Could not load, %s", err_msg);
		
		/* Clean up and return the error. */
		free(err_msg);
		lval_del(a);
		return err;
	}
}


/* Make a function to print. */
lval* builtin_print(lenv* e, lval* a) {
	/* Print the args followed by a space. */
	for (int i = 0; i < a->count; ++i) {
		lval_print(a->cell[i], 1);
		putchar(' ');
	}
	
	/* Print a newline char and clean up. */
	putchar('\n');
	lval_del(a);
	
	return lval_sexpr();
}


/* Make a function to nicely print strings. */
lval* builtin_say(lenv* e, lval* a) {
	/* Print the args. */
	for (int i = 0; i < a->count; ++i) {
		lval_print(a->cell[i], 0);
	}
	
	/* Print a newline char and clean up. */
	putchar('\n');
	lval_del(a);
	
	return lval_sexpr();
}


/* Make a function to throw errors. */
lval* builtin_err(lenv* e, lval* a) {
	LASSERT_NUM("err", a, 1);
	LASSERT_TYPE("err", a, 0, LVAL_STR);
	
	/* Build an error from a given string. */
	lval* err = lval_err(a->cell[0]->str);
	
	/* Clean up and return. */
	lval_del(a);
	return err;
}


/* Make a function to split a string into a list. */
lval* builtin_ssplit(lenv* e, lval* a) {
	LASSERT_NUM("ssplit", a, 1);
	LASSERT_TYPE("ssplit", a, 0, LVAL_STR);
	
	/* Make some vars to keep track of stuff. */
	lval* out = lval_qexpr();
	char* src = a->cell[1]->str;
	char l[2] = { '\0', '\0' };

	
	for (int i = 0; i < strlen(src); ++i) {
		l[0] = src[i];
		out = lval_add(out, lval_str(l));
	}

	lval_del(a);
	return out;
}


/* Make a function to join a list of strings. */
lval* builtin_sjoin(lenv* e, lval* a) {
	for (int i = 0; i < a->count; ++i) {
		LASSERT_TYPE("sjoin", a, 1, LVAL_STR);
	}
	
	/* Hold the joined strings. */
	char* joined = malloc(sizeof(char) * 2);
	memcpy(joined, "", sizeof(char) * 2);
	
	/* Go through each string and put them together. */
	for (int i = 0; i < a->count; ++i) {
		char* str = a->cell[i]->str;
		joined = realloc(joined, strlen(joined) + strlen(str) + 1);
		strcat(joined, str);
	}
	
	lval_del(a);
	lval* out = lval_str(joined);
	free(joined);
	return out;
}


/* Make a function to change a number into a string. */
lval* builtin_num2str(lenv* e, lval* a) {
	LASSERT_NUM("num2str", a, 1);
	LASSERT_TYPE("num2str", a, 0, LVAL_NUM);
	
	char out[80];
	sprintf(out, "%.10g", a->cell[0]->num);
	lval_del(a);
	return lval_str(out);
}


/* Make a function for the builtin add operator. */
lval* builtin_add(lenv* e, lval* a) {
	return builtin_op(e, a, "+");
}


/* Make a function for the builtin subtraction operator. */
lval* builtin_sub(lenv* e, lval* a) {
	return builtin_op(e, a, "-");
}


/* Make a function for the builtin multiplication operator. */
lval* builtin_mul(lenv* e, lval* a) {
	return builtin_op(e, a, "*");
}


/* Make a function for the builtin division operator. */
lval* builtin_div(lenv* e, lval* a) {
	return builtin_op(e, a, "/");
}


/* Make a function for equality comparisons. */
lval* builtin_cmp(lenv* e, lval* a, char* op) {
	LASSERT_NUM(op, a, 2);
	
	int r = 0;
	
	if (strcmp(op, "==") == 0) {
		r = lval_eq(a->cell[0], a->cell[1]);
	}
	if (strcmp(op, "!=") == 0) {
		r = !lval_eq(a->cell[0], a->cell[1]);
	}
	
	lval_del(a);
	return lval_num(r);
}


/* Make a function to check for equality. */
lval* builtin_eq(lenv* e, lval* a) {
	return builtin_cmp(e, a, "==");
}


/* Make a function to check for inequality. */
lval* builtin_ne(lenv* e, lval* a) {
	return builtin_cmp(e, a, "!=");
}


/* Make a function to invert equality checks. */
lval* builtin_not(lenv* e, lval* a) {
	LASSERT_NUM("!", a, 1);
	LASSERT_TYPE("!", a, 0, LVAL_NUM);
	
	int r = !a->cell[0]->num;
	
	lval_del(a);
	return lval_num(r);
}


/* Make function to consolidate numeric ordering checks. */
lval* builtin_ord(lenv* e , lval* a, char* op) {
	LASSERT_NUM(op, a, 2);
	LASSERT_TYPE(op, a, 0, LVAL_NUM);
	LASSERT_TYPE(op, a, 1, LVAL_NUM);
	
	int r = 0;
	if (strcmp(op, ">") == 0) {
		r = (a->cell[0]->num > a->cell[1]->num);
	}
	if (strcmp(op, "<") == 0) {
		r = (a->cell[0]->num < a->cell[1]->num);
	}
	if (strcmp(op, ">=") == 0) {
		r = (a->cell[0]->num >= a->cell[1]->num);
	}
	if (strcmp(op, "<=") == 0) {
		r = (a->cell[0]->num <= a->cell[1]->num);
	}

	lval_del(a);
	return lval_num(r);
}


/* Make a function to check for a greater than value. */
lval* builtin_gt(lenv* e, lval* a) {
	return builtin_ord(e, a, ">");
}


/* Make a function to check for a less than value. */
lval* builtin_lt(lenv* e, lval* a) {
	return builtin_ord(e, a, "<");
}


/* Make a function to check for a greater than or equal value. */
lval* builtin_ge(lenv* e, lval* a) {
	return builtin_ord(e, a, ">=");
}


/* Make a function to check for a less than or equal value. */
lval* builtin_le(lenv* e, lval* a) {
	return builtin_ord(e, a, "<=");
}


/* Make a function to perform conditional execution. */
lval* builtin_if(lenv* e, lval* a) {
	LASSERT_NUM("if", a, 3);
	LASSERT_TYPE("if", a, 0, LVAL_NUM);
	LASSERT_TYPE("if", a, 1, LVAL_QEXPR);
	LASSERT_TYPE("if", a, 2, LVAL_QEXPR);
	
	/* Determine if the first or second arg should be run. */
	int c = a->cell[0]->num
		? 1
		: 2;
	
	/* Change from Q-Expression to S-Expression, then eval it. */
	a->cell[c]->type = LVAL_SEXPR;
	lval* x = lval_eval(e, lval_pop(a, c));
	
	lval_del(a);
	return x;
}


/* Make a function for the builtin lambda operator. */
lval* builtin_lambda(lenv* e, lval* a) {
	/* Check the arguments. */
	LASSERT_NUM("lambda", a, 2);
	LASSERT_TYPE("lambda", a, 0, LVAL_QEXPR);
	LASSERT_TYPE("lambda", a, 1, LVAL_QEXPR);
	
	/* Make sure the first Q-Expression only has symbols. */
	for (int i = 0; i < a->cell[0]->count; ++i) {
		LASSERT_TYPE("lambda", a->cell[0], i, LVAL_SYM);
	}
	
	/* Get the first two args and pass them to lval_lambda. */
	lval* formals = lval_pop(a, 0);
	lval* body = lval_pop(a, 0);
	lval_del(a);
	
	return lval_lambda(formals, body);
}


/* Make a function to allow the language to make new functions. */
lval* builtin_var(lenv* e, lval* a, char* func) {
	LASSERT_TYPE(func, a, 0, LVAL_QEXPR);
	
	/* First element is a symbol list. */
	lval* syms = a->cell[0];
	
	/* Make sure all of the elements of the first list are symbols. */
	for (int i = 0; i < syms->count; ++i) {
		LASSERT_TYPE(func, syms, i, LVAL_SYM);
	}
	
	/* Check for a correct number of symbols and values. */
	LASSERT_NUM(func, syms, a->count - 1);
	
	/* Assign copies of values to symbols. */
	for (int i = 0; i < syms->count; ++i) {
		if (strcmp(func, "def") == 0) {
			lenv_def(e, syms->cell[i], a->cell[i + 1]);
		}
		
		if (strcmp(func, "=") == 0) {
			lenv_put(e, syms->cell[i], a->cell[i + 1]);
		}
	}
	
	lval_del(a);
	return lval_sexpr();
}


/* Make a builtin function to put the var in the local env. */
lval* builtin_put(lenv* e, lval* a) {
	return builtin_var(e, a, "=");
}


/* Make a builtin function to put the var in the root env. */
lval* builtin_def(lenv* e, lval* a) {
	return builtin_var(e, a, "def");
}


/* Make a function to spit out a list of the current environment vars. A true 
arg determines if it gives just the root env. */
lval* builtin_env(lenv* e, lval* a) {
	LASSERT_NUM("env", a, 1);
	LASSERT_TYPE("env", a, 0, LVAL_NUM);
	
	lval* envlist = lval_qexpr();

	if (a->cell[0]->num) {
		while (e->par != NULL) { e = e->par; }
	}
	
	/* Iterate over the items in the environment for each env. */
	while (e != NULL) {
		for (int i = 0; i < e->count; ++i) {
			lval_add(envlist, lval_sym(e->syms[i]));
		}
		e = e->par;
	} ;
	
	return envlist;
}


lval* lval_eval_sexpr(lenv* e, lval* v) {
	/* Eval children. */
	for (int i = 0; i < v->count; ++i) {
		v->cell[i] = lval_eval(e, v->cell[i]);
	}
	
	/* Error Checking. */
	for (int i = 0; i < v->count; ++i) {
		if (v->cell[i]->type == LVAL_ERR) {
			return lval_take(v, i);
		}
	}
	
	/* If the expression is empty. */
	if (v->count == 0) { return v; }
	
	/* If it's a single expression. */
	if (v->count == 1) { return lval_take(v, 0); }
	
	/* Make sure the first element is a symbol. */
	lval* f = lval_pop(v, 0);
	if (f->type != LVAL_FUN) {
		lval_del(f);
		lval_del(v);
		return lval_err("S-Expression does not start with a function.");
	}
	
	/* Call the builtin with the operator. */
	lval* result = lval_call(e, f, v);
	lval_del(f);
	return result;
}


/* Evaluate S-Expressions, but leave everything else the same. */
lval* lval_eval(lenv* e, lval* v) {
	if (v->type == LVAL_SYM) {
		lval* x = lenv_get(e, v);
		lval_del(v);
		return x;
	}
	if (v->type == LVAL_SEXPR) { return lval_eval_sexpr(e, v); }
	else { return v; }
}


/* Define the main loop and the parsing of the language. */
int main(int args, char** argv) {
	/* Make linenoise use multiple lines instead of tring to push the line
	back. */
	linenoiseSetMultiLine(1);

	/* Make some parsers for the Lisp. */
	Comment = mpc_new("comment");
	Number = mpc_new("number");
	Symbol = mpc_new("symbol");
	String = mpc_new("string");
	Sexpr = mpc_new("sexpr");
	Qexpr = mpc_new("qexpr");
	Expr = mpc_new("expr");
	Lispy = mpc_new("lispy");

	/* Define the parsers. */
	mpca_lang(MPCA_LANG_DEFAULT,
		" \
			number	: /-?[0-9]+(\\.[0-9]+)?/ ; \
			symbol	: /[a-zA-Z0-9_+\\-*\\/\\\\=<>!&]+/ ; \
			string	: /\"(\\\\.|[^\"])*\"/ ; \
			comment	: /[;#][^\\r\\n]*/ ; \
			sexpr	: '(' <expr>* ')' ; \
			qexpr	: '{' <expr>* '}' ; \
			expr	: <number> | <symbol> | <string> \
					| <comment> | <sexpr> | <qexpr> ; \
			lispy	: /^/ <expr>* /$/ ; \
		",
		Comment, Number, Symbol, String, Sexpr, Qexpr, Expr, Lispy);
	
	
	/* Make the environment and add the builtin functions to it. */
	lenv* e = lenv_new();
	lenv_add_builtins(e);
		
	
	/* Make a var to determine interactivity. */
	int inter = 0;
	
	if (args >= 2) {
	
		/* For each argument given, check if it's a flag. Otherwise assume
		its a file. */
		for (int i = 1; i < args; ++i) {
		
			/* Interactive flag. */
			if (strcmp(argv[i], "-i") == 0) { inter = 1; continue; }
			
			/* Make an arg list with only one arg. */
			lval* args = lval_add(lval_sexpr(), lval_str(argv[i]));
			
			/* Pass it to the loading function. */
			lval* x = builtin_load(e, args);
			
			/* Print the result if it's an error. */
			if (x->type == LVAL_ERR) { lval_println(x, 1); }
			lval_del(x);
		}
	}
	
	if ((args == 1) || (inter)) {
	
		/* Print the version and exiting info. */
		puts("Lispish v" VERSION);
		puts("Press Ctrl+D to Exit\n");

		/* Infinite loop */
		char* input;
		while ( input = linenoise("lispish> ") ) {
			/* Make a var to hold the parsed results. */
			mpc_result_t r;
			
			/* printf("Got: '%s'\n", input); */
			
			/* Add the input to the history. */
			linenoiseHistoryAdd(input);
	
			/* Attempt to parse the user's input. */
			if (mpc_parse("<stdin>", input, Lispy, &r)) {
				/* mpc_ast_print(r.output); */
				
				lval* x = lval_eval(e, lval_read(r.output));
				lval_println(x, 1);
				lval_del(x);
				
				mpc_ast_delete(r.output);
			} else {
				mpc_err_print(r.error);
				mpc_err_delete(r.error);
			}
			
			/* Free the memory made by linenoise. */
			free(input);
		}
	}
	
	/* Clean up the environment. */
	lenv_del(e);

	/* Clean up the parser. */
	mpc_cleanup(8,
		Comment,
		Number,
		Symbol,
		String,
		Sexpr,
		Qexpr,
		Expr,
		Lispy);

	return 0;
}
