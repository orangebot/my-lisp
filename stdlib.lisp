; Basic atoms
(def {nil} {})
(def {true} 1)
(def {false} 0)




; Building blocks

; Define functions easier.
(def {func}
	(fun {name body}
		{def (head name) (fun (tail name) body)}))

; Define local vars much easier.
(func {let b}
	{( (fun {_} b) () )})

; Do a set of things in order.
(func {do & l}
	{if (== l nil)
		{nil}
		{last l}})

; Unpack a list for a function.
(func {unpack f l}
	{eval (join (list f) l)})
; Make an alias for unpacking.
(def {curry} unpack)

; Pack a list for a function.
(func {pack f & xs} {f xs})
; Make an alias for packing.
(def {uncurry} pack)




; Logical Operators
(func {not x} {! x})
(func {or x y} {+ x y})
(func {and x y} {* x y})




; List functions

; Return the nth item of a list.
(func {fst l} {eval (head l)})
(func {snd l} {eval (head (tail l))})
(func {trd l} {eval (head (tail (tail l)))})
(func {nth n l}
	{if (== n 0)
		{fst l}
		{nth (- n 1) (tail l)}})

; Merge a list with a given base, using a function.
(func {foldl f z l}
	{if (== l nil)
		{z}
		{foldl f (f z (fst l)) (tail l)}})

; Return the length of a structure.
(func {len l}
	{foldl (fun {x y} {+ x 1}) 0 l})

; Return the last element in a list
(func {last & l}
	{nth (- (len l) 1) l})

; Find element in a list.
(func {elem x l}
	{if (== l nil)
		{false}
		{if (== x (fst l))
			{true}
			{elem x (tail l)}}})

; Apply a function to each item in a list.
(func {map f l}
	{if (== l nil)
		{nil}
		{join (list (f (fst l))) (map f (tail l))}})

; Grep for particular items in a list.
(func {grep f l}
	{if (== l nil)
		{nil}
		{join
			(if (f (fst l))
				{head l}
				{nil})
			(grep f (tail l))}})

; Replicate a value 'm' number of times.
(func {dup v m}
	{if (== m 0)
		{nil}
		{join (list v) (dup v (- m 1))}})

; Reverse a list
(func {rev l}
	{foldl (fun {a b} {join (list b) a}) nil l })

; Make a sequence of numbers
(func {seq a b}
	{select
		{ (== a b) (list a) }
		{ (< a b) (join (list a) (seq (+ a 1) b)) }
		{ (> a b) (rev (seq b a)) } })




; Math functions

; Sum of a list of nums
(func {sum l}
	{foldl + 0 l})

; Product of a list of nums
(func {product l}
	{foldl * 1 l})

; The exponent of a value
(func {pow v n}
	{if (> n 0)
		{foldl * 1 (dup v n)}
		{/ 1 (foldl * 1 (dup v (* n -1)))}})

; Fibonacci
(func {fib n}
	{select
		{(== n 0) 0}
		{(== n 1) 1}
		{otherwise
			(+ (fib (- n 1)) (fib (- n 2)))}})




; Conditional functions

; Switch-like structure
(func {select & cs}
	{if (== cs nil)
		{err "No selection found"}
		{if (fst (fst cs))
			{snd (fst cs)}
			{unpack select (tail cs)}}})

; Make a switch/case like structure closer to the C implimentation
(func {case x & cs}
	{if (== cs nil)
		{err "No case found"}
		{if (== x (fst (fst cs)))
			{snd (fst cs)}
			{unpack case (join (list x) (tail cs))}}})

; Default case for a select.
(def {otherwise} true)
