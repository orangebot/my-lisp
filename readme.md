# Make a Lisp

This project follows the tutorial found at [BuildYourOwnLisp.com][1].

## Why?

Yes, I know there are a bananas amount of Lisps, but I figured it would be fun, and I had a couple of days free.

## What's next?

I have since finished the tutorial, but there were some problems I had with the system. Namely:

- Strings are difficult/confusing to work with.
- Numbers are constrained to C's double size.
- No way to work with files.
- Is it even worth messing with networks?
- Where do I store libraries?

I would like to try to address this at some point. Particularly the difficulty about working with and manipulating strings.


[1]: http://www.buildyourownlisp.com
