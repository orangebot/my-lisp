#Config file for make

VERSION = 1.3.2

#Where to install the bin file
PREFIX = /usr/local

#Compilation flags to pass
CCFLAGS = -DVERSION=\"$(VERSION)\" $(CFLAGS)

#Linking flags to pass
CLDFLAGS = -std=c99 -Wall -lm $(LDFLAGS)

#C Compiler to use
#CC = tcc

#Where to write test files
TESTTMP = /tmp/my-lisp
